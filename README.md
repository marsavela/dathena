# CICD flow for Dathena Interview
This sets up a developing environment of two very simple webservers.

## Dependencies
* Docker
* Vagrant
* make
* git

## Usage
```sh
git clone https://marsavela@bitbucket.org/marsavela/dathena.git 
cd dathena
make
```

At this point, you already should have docker running a pre-configured Jenkins container and a Vagrant VM used as "production" in order to deploy the following webservices:
* https://marsavela@bitbucket.org/marsavela/simple-api-python.git
* https://marsavela@bitbucket.org/marsavela/simple-api-scala.git

Python webservice runs on port 5000 and scala webservice runs on port 8080 (default ports). Vagrant machine should be accessible through the IP `192.168.33.11`. Jenkins can be accessible at http://localhost:8080 and both jobs are set to poll the repositories for changes every 10 minutes.

`Dockerfile`s and `Jenkinsfile`s are in its respective repository.

## Important URLs
* Jenkins: http://localhost:8080
* Python Webservice: http://192.168.33.11:5000
* Scala Webservice: http://192.168.33.11:8080

## Clean
In order to destroy everything just run
```sh
make clean
```

## TODOs
* Checking for the Scala Webservice status code from within the pipeline.
    * Connection keeps being refused.
