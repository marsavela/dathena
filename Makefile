mkfile_dir := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

init:
	vagrant up --provision
	docker build -t jenkinsdockersbt .
	docker create --name jenkins -p 8080:8080 -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v ${mkfile_dir}:/automation jenkinsdockersbt
	docker run --rm --volumes-from jenkins -v ${mkfile_dir}:/backup ubuntu bash -c "cd /var/jenkins_home && rm -rf ./* && tar xvf /backup/jenkins_home.tar --strip 2"
	docker start jenkins

save_state:
	docker stop jenkins
	docker run --rm --volumes-from jenkins -v ${mkfile_dir}:/backup ubuntu tar cvf /backup/jenkins_home.tar /var/jenkins_home
	docker start jenkins

clean:
	docker stop jenkins || true
	docker rm jenkins || true
	docker volume rm jenkins_home || true
	docker image rm jenkinsdockersbt || true
	vagrant destroy -f
	rm -rf ${mkfile_dir}/.vagrant
